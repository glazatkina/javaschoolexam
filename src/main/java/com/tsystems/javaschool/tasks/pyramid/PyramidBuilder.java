package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
            int height = getHeight(inputNumbers.size());
            int width = getWidth(height);

            int[][] res = new int[height][width];
            Iterator<Integer> iterator = inputNumbers.iterator();

            for (int i = 0; i < height; i++) {
                Arrays.fill(res[i], 0);
                int level = i + 1;
                int index = (width - (2 * level - 1)) / 2;

                for (int j = 0; j < level; j++) {
                    res[i][index] = iterator.next();
                    index += 2;
                }
            }

            return res;
        } catch (Throwable e) {
            throw new CannotBuildPyramidException();
        }
    }

    private int getHeight(int length) {
        if (length == 0) throw new CannotBuildPyramidException();
        int res = 1;
        int current = 1;

        while (res < length) {
            current++;
            res += current;
        }

        if (res != length) throw new CannotBuildPyramidException();
        return current;
    }

    private int getWidth(int height) {
        return 2 * height - 1;
    }

}
