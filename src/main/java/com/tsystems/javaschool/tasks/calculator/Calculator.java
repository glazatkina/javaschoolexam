package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    public static void main(String[] args) {
        String input = "22/3*3.0480";
        Calculator calc = new Calculator();
        System.out.println(calc.evaluate(input));
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            ArrayList<String> calculationList = makeReversePolish(statement);
            return calculatePolish(calculationList);
        } catch (Exception e) {
            return null;
        }
    }

    private ArrayList<String> makeReversePolish(String statement) {
        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> stack = new ArrayList<>();

        StringBuilder number = new StringBuilder("");

        for (char ch : statement.toCharArray()) {
            if (isOpeningBracket(ch)) {
                if (number.length() > 0) {
                    if (number.charAt(number.length() - 1) == '.') throw new IllegalArgumentException();
                    res.add(number.toString());
                    number.setLength(0);
                }
                stack.add("(");
                continue;
            }

            if (isClosingBracket(ch)) {
                if (number.length() == 0) throw new IllegalArgumentException();

                if (number.charAt(number.length() - 1) == '.') throw new IllegalArgumentException();
                res.add(number.toString());
                number.setLength(0);

                boolean haveOpening = false;
                while (!stack.isEmpty()) {
                    String str = stack.remove(stack.size() - 1);
                    if (str.equals("(")) {
                        haveOpening = true;
                        break;
                    } else {
                        res.add(str);
                    }
                }

                if (!haveOpening) throw new IllegalArgumentException();
                continue;
            }

            if (isOperator(ch)) {
                if (number.length() > 0) {
                    if (number.charAt(number.length() - 1) == '.') throw new IllegalArgumentException();
                    res.add(number.toString());
                    number.setLength(0);
                }

                while (!stack.isEmpty() && getPriority(ch) <= getPriority(stack.get(stack.size() - 1).charAt(0))) {
                    res.add(stack.remove(stack.size() - 1));
                }
                stack.add(Character.toString(ch));
                continue;
            }

            if (isDigit(ch)) {
                number.append(ch);
                continue;
            }

            if (isDot(ch)) {
                if (number.length() > 0) {
                    if (isDot(number.charAt(number.length() - 1))) throw new IllegalArgumentException();
                    number.append(ch);
                } else {
                    throw new IllegalArgumentException();
                }
                continue;
            }

            throw new IllegalArgumentException();
        }

        if (number.length() > 0) {
            if (isDot(number.charAt(number.length() - 1))) throw new IllegalArgumentException();
            res.add(number.toString());
        }

        while (!stack.isEmpty()) {
            res.add(stack.remove(stack.size() - 1));
        }

        return res;
    }

    private boolean isOpeningBracket(char ch) {
        return ch == '(';
    }

    private boolean isClosingBracket(char ch) {
        return ch == ')';
    }

    private boolean isDigit(char ch) {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(Character.toString(ch));
        return m.matches();
    }

    private boolean isDot(char ch) {
        return ch == '.';
    }

    private boolean isOperator(char ch) {
        Pattern p = Pattern.compile("[-+/*]");
        Matcher m = p.matcher(Character.toString(ch));
        return m.matches();
    }

    private int getPriority(char operator) {
        if (operator == '*' || operator == '/') return 2;
        if (operator == '+' || operator == '-') return 1;
        if (operator == '(' || operator == ')') return 0;
        return -1;
    }

    private String calculatePolish(List<String> operands) {
        try {
            Deque<String> stack = new ArrayDeque<>();
            for (String operand : operands) {
                if (operand.length() == 1 && isOperator(operand.charAt(0))) {
                    String number2 = stack.pop();
                    String number1 = stack.pop();
                    stack.push(applyOperation(number1, number2, operand));
                } else {
                    stack.push(operand);
                }
            }

            if (stack.size() != 1) return null;
            return stack.getLast();
        } catch (Exception e) {
            return null;
        }
    }

    private String applyOperation(String operand1, String operand2, String operation) {

        double a = Double.parseDouble(operand1);
        double b = Double.parseDouble(operand2);

        boolean isResultInteger = false;

        if (!operand1.contains(".") && !operand2.contains(".")) {
            isResultInteger = true;
        }

        String res = "";
        switch (operation) {
            case "+":
                a += b;
                break;
            case "-":
                a -= b;
                break;
            case "*":
                a *= b;
                break;
            case "/":
                if (isResultInteger && Integer.parseInt(operand1) % Integer.parseInt(operand2) != 0) {
                    isResultInteger = false;
                }
                a /= b;
                break;
        }


        if (isResultInteger) {
            res = Long.toString(Math.round(a));
        } else {
            res = Double.toString(a);
        }

        return res;
    }
}
