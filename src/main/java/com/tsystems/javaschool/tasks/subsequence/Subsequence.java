package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        if (x.size() > y.size()) return false;

        ListIterator first = x.listIterator();
        ListIterator second = y.listIterator();

        while (first.hasNext()) {
            Object search = first.next();
            boolean isHere = false;
            while (second.hasNext()) {
                if (search.equals(second.next())) {
                    isHere = true;
                    break;
                }
            }
            if (!isHere) return false;
        }
        return true;
    }
}
